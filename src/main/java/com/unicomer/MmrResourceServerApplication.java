package com.unicomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmrResourceServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmrResourceServerApplication.class, args);
	}

}
