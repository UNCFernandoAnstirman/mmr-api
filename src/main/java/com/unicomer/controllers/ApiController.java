package com.unicomer.controllers;

import java.util.LinkedList;

import com.unicomer.model.NewEvent;
import com.unicomer.service.EventService;
import com.unicomer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.graph.models.Event;
import com.microsoft.graph.models.User;
import com.microsoft.graph.options.HeaderOption;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.requests.GraphServiceClient;
import com.unicomer.aad.OboAuthProvider;

import okhttp3.Request;

@RestController
public class ApiController {

	@Autowired
	OboAuthProvider oboAuthProvider;

	@Autowired
	EventService eventService;

	@Autowired
	UserService userService;

	@RequestMapping("/api/test")
	public String test() {
		return "Test info";
	}

	@RequestMapping("/api/info")
	public ResponseEntity<String> graphMeApi() {

		try {
			GraphServiceClient<Request> graphClient = GraphServiceClient.builder()
					.authenticationProvider(oboAuthProvider).buildClient();

			User user = graphClient.me().buildRequest().get();

			ObjectMapper objectMapper = new ObjectMapper();
			return ResponseEntity.status(200).body(objectMapper.writeValueAsString(user));
		} catch (Exception ex) {
			System.out.print("ERROR TRYING TO FETCH GRAPH DATA: " + ex);
			return ResponseEntity.status(500).body(String.format("%s: %s", ex.getCause(), ex.getMessage()));
		}
	}


	@PostMapping("/api/events")
	public ResponseEntity<String> createEvent(@RequestBody NewEvent newEvent){

		GraphServiceClient<Request> graphClient = GraphServiceClient.builder()
				.authenticationProvider(oboAuthProvider).buildClient();

		try{

			Event event = eventService.createEvent(newEvent);

			LinkedList<Option> requestOptions = new LinkedList<Option>();
			requestOptions.add(new HeaderOption("Prefer", "outlook.timezone=\"Pacific Standard Time\""));

			Event confirmedEvent = graphClient.me().events()
					.buildRequest( requestOptions )
					.post(event);

			ObjectMapper objectMapper = new ObjectMapper();
			return ResponseEntity.status(200).body(objectMapper.writeValueAsString(confirmedEvent));
		}catch(Exception ex){
			System.out.print("ERROR TRYING TO CREATE NEW EVENT: " + ex);
			return ResponseEntity.status(500).body(String.format("%s: %s", ex.getCause(), ex.getMessage()));
		}

	}

}
