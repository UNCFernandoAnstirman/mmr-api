package com.unicomer.controllers;

import com.microsoft.graph.models.User;
import com.microsoft.graph.requests.ContactCollectionPage;
import com.unicomer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/api/user/{principalName}")
    public Object getUserByEmail(@PathVariable String principalName){


        User user = userService.getUser(principalName);
        if(user != null){
            return user;
        }else{
            HashMap<String,String> map = new HashMap<>();
            map.put("message","Unable to get specified user");
            return map;
        }
    }

    @GetMapping("/api/user/contacts")
    public Object getContacts(){
        ContactCollectionPage contacts = userService.getContacts();
        if(contacts != null){
            return contacts;
        }else{
            HashMap<String,String> map = new HashMap<>();
            map.put("message","Unable to get contact list");
            return map;
        }
    }

}
