package com.unicomer.service.impl;

import com.microsoft.graph.models.*;
import com.unicomer.model.NewEvent;
import com.unicomer.service.EventService;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {
    @Override
    public Event createEvent(NewEvent newEvent) {
        Event event = new Event();

        event.subject = newEvent.getSubject();

        DateTimeTimeZone start = new DateTimeTimeZone();
        start.dateTime = newEvent.getStartTime();
        start.timeZone = newEvent.getTimezone();
        event.start = start;

        DateTimeTimeZone end = new DateTimeTimeZone();
        end.dateTime = newEvent.getEndTime();
        end.timeZone = newEvent.getTimezone();
        event.end = end;

        Location location = new Location();
        location.displayName = newEvent.getLocationName();
        event.location = location;

        LinkedList<Attendee> attendees = new LinkedList<Attendee>();
        List<String> attendeeList = newEvent.getAttendees();
        for(String attendeeEmail: attendeeList){
            Attendee newAttendee = new Attendee();

            EmailAddress emailAddress = new EmailAddress();
            emailAddress.address = attendeeEmail;

            newAttendee.emailAddress = emailAddress;
            newAttendee.type = AttendeeType.REQUIRED;

            attendees.add(newAttendee);

        }
        event.attendees = attendees;
        event.allowNewTimeProposals = true;
        event.isOnlineMeeting = true;
        event.onlineMeetingProvider = OnlineMeetingProviderType.TEAMS_FOR_BUSINESS;

        return event;
    }
}
