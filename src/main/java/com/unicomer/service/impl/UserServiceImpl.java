package com.unicomer.service.impl;

import com.microsoft.graph.models.User;
import com.microsoft.graph.requests.ContactCollectionPage;
import com.microsoft.graph.requests.GraphServiceClient;
import com.unicomer.aad.OboAuthProvider;
import com.unicomer.service.UserService;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    OboAuthProvider oboAuthProvider;

    private GraphServiceClient graphClient;
    @PostConstruct
    public void initialize(){
        this.graphClient = GraphServiceClient.builder()
                .authenticationProvider(oboAuthProvider).buildClient();
    }

    @Override
    public User getUser(String principalName) {
        User user = null;
        try{
            user = graphClient.users(principalName)
                    .buildRequest()
                    .get();

            return user;
        }catch (Exception ex){
            System.out.println("ERROR TRYING TO GET USER BY EMAIL: "+ex);
            return user;
        }
    }

    @Override
    public ContactCollectionPage getContacts() {
        ContactCollectionPage contacts = null;
        try{
            contacts = graphClient.me().contacts().buildRequest().get();
        }catch(Exception e){
            System.out.println("ERROR TRYING TO FETCH CONTACTS LIST: "+e);
        }

        return contacts;
    }
}
