package com.unicomer.service;

import com.microsoft.graph.models.User;
import com.microsoft.graph.requests.ContactCollectionPage;

public interface UserService {

    public User getUser(String principalName);
    public ContactCollectionPage getContacts();
}
