package com.unicomer.service;

import com.microsoft.graph.models.Event;
import com.unicomer.model.NewEvent;

public interface EventService {
    public Event createEvent(NewEvent newEvent);
}
