package com.unicomer.model;

import java.util.List;

public class NewEvent {

    private String subject;
    private String timezone;
    private String startTime;
    private String endTime;
    private boolean isOnlineMeeting;
    private String locationName;
    private List<String> attendees;

    public NewEvent(String subject, String timezone, String startTime, String endTime, String locationName, List<String> attendees) {
        this.subject = subject;
        this.timezone = timezone;
        this.startTime = startTime;
        this.endTime = endTime;
        this.locationName = locationName;
        this.attendees = attendees;
        this.isOnlineMeeting = true;
    }

    public String getSubject() {
        return subject;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public boolean isOnlineMeeting() {
        return isOnlineMeeting;
    }

    public String getLocationName() {
        return locationName;
    }

    public List<String> getAttendees() {
        return attendees;
    }
}
