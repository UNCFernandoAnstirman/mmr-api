package com.unicomer.config;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;

@Configuration
public class APIConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	  protected void configure(HttpSecurity http) throws Exception {
	    http.oauth2ResourceServer(
	            c -> c.jwt(
	                    j -> j.decoder(decoder())
	            )
	    );

	    http
	    		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	    		.and()
	    		.authorizeRequests()
	    		.antMatchers("/api/**").hasAuthority("SCOPE_access_as_user")
	            .anyRequest().authenticated();
	  }
	
	
	
	@Bean
	  public JwtDecoder decoder() {
	    return 
	    		NimbusJwtDecoder.withJwkSetUri("https://login.microsoftonline.com/common/discovery/v2.0/keys").build();
	            
	  }

}
